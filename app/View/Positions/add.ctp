<!-- File: /app/View/Positions/add.ctp -->

<h1>Adicionar Posição</h1>
<?php
echo $this->Form->create('Position');
echo $this->Form->input('bay_number',array('label' => 'Número da Baia'));
echo $this->Form->input('location_id',array('label' => 'Localidade','options'=>$locations));
echo $this->Form->input('px',array('label' => 'px'));
echo $this->Form->input('py',array('label' => 'px'));
echo $this->Form->input('width',array('label' => 'width'));
echo $this->Form->input('height',array('label' => 'height'));

echo $this->Form->end('Salvar');
?>
