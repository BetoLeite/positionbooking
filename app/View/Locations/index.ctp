<h1>Localidades</h1>
    <?php echo $this->Html->link(
        'Nova Localidade',
        array('controller'=>'locations','action'=>'add')
    ); ?>
<table>
    <tr>
        <th>Nome</th>
        <th>Endereço</th>
	<th>URI Planta</th>
        <th>Ações</th> 
	    <th>Criação</th>
	    <th>Modificação</th>
    </tr>


    <?php foreach ($locations as $location): ?>
    <tr>
	<td><?php echo $location['Location']['name']; ?></td>
	<td><?php echo $location['Location']['address']; ?></td>
 	<td><?php echo $location['Location']['image_url']; ?></td>
    <td>
        <?php
            echo $this->Html->link(
                'Edit', array('action' => 'edit', $location['Location']['id'])
            );
        ?>
        <?php
            echo $this->Html->link(
                'Delete', array('action' => 'delete', $location['Location']['id'])      
            );
        ?>

        </td>
	<td><?php echo $location['Location']['created']; ?></td> 
	<td><?php echo $location['Location']['modified']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($location); ?>
</table>
