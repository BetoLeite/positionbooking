<h1>Configurações</h1>
    <?php echo $this->Html->link(
        'Nova Configuração',
        array('controller'=>'configurations','action'=>'add')
    ); ?>
<table>
    <tr>
        <th>Chave</th>
        <th>Valor</th>
        <th>Ações</th> 
	    <th>Criação</th>
	    <th>Modificação</th>
    </tr>


    <?php foreach ($configurations as $configuration): ?>
    <tr>
	<td><?php echo $configuration['Configuration']['key']; ?></td>
	<td><?php echo $configuration['Configuration']['value']; ?></td>
    <td>
        <?php
            echo $this->Html->link(
                'Edit', array('action' => 'edit', $configuration['Configuration']['id'])
            );
        ?>
        <?php
            echo $this->Html->link(
                'Delete', array('action' => 'delete', $configuration['Configuration']['id'])      
            );
        ?>

        </td>
	<td><?php echo $configuration['Configuration']['created']; ?></td> 
	<td><?php echo $configuration['Configuration']['modified']; ?></td>
    </tr>
    <?php endforeach; ?>
    <?php unset($configuration); ?>
</table>
