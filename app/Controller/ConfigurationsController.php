<?php
class ConfigurationsController extends AppController {
    public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('configurations', $this->Configuration->find('all'));
    }

    public function add(){
        if ($this->request->is('post')){
            $this->Configuration->create();
            if ($this->Configuration->save($this->request->data)){
                $this->Session->setFlash(__('Configuration has been saved'));
                return $this->redirect(array('action'=> 'index'));
            }
            $this->Session->setFlash(__('Unable to save the Configuration'));
        }
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid Configuration'));
        }

        $configuration = $this->Configuration->findById($id);
        if (!$configuration) {
            throw new NotFoundException(__('Invalid Configuration'));
        }

        if ($this->request->is(array('configuration', 'put'))) {
            $this->Configuration->id = $id;
            if ($this->Configuration->save($this->request->data)) {
                $this->Session->setFlash(__('Your Configuration has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to update the Configuration.'));
        }

        if (!$this->request->data) {
            $this->request->data = $configuration;
        }
    }    	

    public function delete($id = null) {
  //      if ($this->request->is('get')) {
  //          throw new MethodNotAllowedException();
  //      }

        if ($this->Configuration->delete($id)) {
            $this->Session->setFlash(
                __('The Configuration %s has been deleted.', h($id))
            );
            return $this->redirect(array('action' => 'index'));
        }
    }
}
?>
