<?php
class LocationsController extends AppController {
    public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('locations', $this->Location->find('all'));
    }

    public function add(){
        if ($this->request->is('post')){
            $this->Location->create();
            if ($this->Location->save($this->request->data)){
                $this->Session->setFlash(__('Location has been saved'));
                return $this->redirect(array('action'=> 'index'));
            }
            $this->Session->setFlash(__('Unable to save the Location'));
        }
    }

    public function edit($id = null) {
        if (!$id) {
            throw new NotFoundException(__('Invalid Location'));
        }

        $location = $this->Location->findById($id);
        if (!$location) {
            throw new NotFoundException(__('Invalid Location'));
        }

        if ($this->request->is(array('location', 'put'))) {
            $this->Location->id = $id;
            if ($this->Location->save($this->request->data)) {
                $this->Session->setFlash(__('Your Location has been updated.'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(__('Unable to update the Location.'));
        }

        if (!$this->request->data) {
            $this->request->data = $location;
        }
    }    	

    public function delete($id = null) {
  //      if ($this->request->is('get')) {
  //          throw new MethodNotAllowedException();
  //      }

        if ($this->Location->delete($id)) {
            $this->Session->setFlash(
                __('The Location %s has been deleted.', h($id))
            );
            return $this->redirect(array('action' => 'index'));
        }
    }
}
?>
